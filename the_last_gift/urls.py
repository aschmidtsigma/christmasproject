"""the_last_gift URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from django.contrib import admin
from story.views import index, start, final, secret, OptionAjax, PlayView, InventoryAjax, ItemAjax, MakeView

urlpatterns = [
    path('adminsigma/', admin.site.urls),
    path('', index, name='index'),
    path('start/', start, name='start'),
    path('play/', PlayView.as_view(), name='play'),
    path('final/', final, name='final'),
    path('secret/', secret, name='secret'),
    path('make/', MakeView.as_view(), name='make'),
    path('play/ajax/option', OptionAjax.as_view(), name='option'),
    path('play/ajax/inventory', InventoryAjax.as_view(), name='inventory'),
    path('play/ajax/item', ItemAjax.as_view(), name='item')
]
