$(document).ready(function() {

    const infoHeight = $('#info-background').css('height');
    $('#inventory-button').hover(function () {
        $(this)[0].src = '../static/images/bag_white.png';
    }, function () {
        $(this)[0].src = '../static/images/bag_red.png';
    }
    );

    $('#close-inventory-button').hover(function () {
        $(this)[0].src = '../static/images/close_white.png';
    }, function () {
        $(this)[0].src = '../static/images/close_red.png';
    }
    );

    $('#inventory-button').click(function () {
        $('#info-background').animate(
            {
                'height': '75%'
            }, {
                duration: 750,
                easing: 'swing',
                start: function () {
                    $('#info-background').css('min-height', infoHeight);
                    $('#info-background').toggleClass('inventory')
                },
                done: function () {
                    getItems();
                }
            });
    });

    $('#close-inventory-button').click(function () {
        $('#info-background').animate({'height': infoHeight}, {
            duration: 750,
            easing: 'swing',
            complete: function() {
                $('#info-background').toggleClass('inventory');
            }
        })
    })
})

function getItems() {
    $.ajax({
        url: '/play/ajax/inventory',
        success: function (data) {
            $('#items').remove();
            $('#info-background').append("<div id='items'></div>");
            data.items.forEach(item => {
                $('#info-background').find('#items').append(`
                    <img data-item='${item.id}' class='item' src='../static/images/items/${item.image}' aria-describedby='tooltip'/>
                    <div data-item='${item.id}' class='tool' role='tooltip'>${item.name}</div>
                `)
            });
            setupTooltips();
        }
    })
}