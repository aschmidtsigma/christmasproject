$(document).ready(function () {
  const gameAudio = $("#game-audio")[0];

  $("#audio-button").hover(
    function () {
      $(this)[0].src = gameAudio.muted
        ? "../static/images/mute_white.png"
        : "../static/images/unmute_white.png";
    },
    function () {
      $(this)[0].src = gameAudio.muted
        ? "../static/images/mute_red.png"
        : "../static/images/unmute_red.png";
    }
  );

  $("#audio-button").click(function () {
    $(this)[0].src = gameAudio.muted
        ? "../static/images/unmute_white.png"
        : "../static/images/mute_white.png";

    gameAudio.volume = 0;

    $("#game-audio").animate(
      { volume: 0.07 },
      {
        duration: 5000,
        easing: "swing",
        start: function () {
          $("#game-audio").trigger("load").trigger("play");
        },
      }
    );
    gameAudio.muted = !gameAudio.muted;
  });
});
