let poppers = {}
function setupTooltips() {
    poppers = {};
    $('.item').each(function (_index) {
        id = $(this).attr('data-item');
        tool = $(`.tool[data-item='${id}']`)[0];
        poppers[id] = Popper.createPopper($(this)[0], tool, {
            modifiers: [
              {
                name: 'offset',
                options: {
                  offset: [0, 10],
                },
              },
            ],
          })
    })

    $('.item').hover(
        function () {
            id = $(this).attr('data-item');
            $(`.tool[data-item='${id}']`).attr('data-show', '')
            poppers[id].update()
        },
        function () {
            id = $(this).attr('data-item');
            $(`.tool[data-item='${id}']`).removeAttr('data-show')
        }
    )

    $('.item').click(function () {
      $.ajax({
        url: '/play/ajax/item',
        data: {
          item: $(this).attr('data-item'),
        },
        success: function (data) {
          $('body').append(`
            <section id='item-popup' class="popup vertical full">
            </section>
          `);
          $('#item-popup').append(`
            <div id='item-info' class='box vertical'>
              <h3>${data.blurb}</h3>
              <p>${data.words}</p>
            </div>
            <div id='item-control' class='horizontal'>
              <button id='item-exit'>RETURN</button>
            </div>
          `)
          if (data.is_letter) {
            $('#item-control').append(`
            <button id='letter-get'>PRINT</button>
            `);
          }
          $('#item-popup').click(function() {
            $('#item-popup').remove();
          })
          $('#item-exit').click(function() {
            $('#item-popup').remove();
          })

          $('#letter-get').click(function() {
            letter = $('#item-info')[0]
            newWindow = window.open("", "LETTER");
            console.log(letter)
            newWindow.document.write(letter.outerHTML)
            newWindow.print();
            newWindow.close();
          })
        }
      })
    })
}
