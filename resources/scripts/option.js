$(".option").click(onOptionClick);

function onOptionClick() {
  $.ajax({
    url: "/play/ajax/option",
    data: {
      option: $(this).attr("data-id"),
      next_scene: $(this).attr("data-next"),
    },
    success: onSuccess,
  });
}

function onSuccess(data) {
  $("#description").find("#blurb").text(data.blurb);
  $("#description").find("#words").text(data.words);
  $("#options").empty();
  if (data.make) {
    window.location.href = '/make';
    return;
  }
  if (data.song) {
    $("#game-audio")
      .find("source")
      .attr("src", `/static/audio/${data.song}.mp3`);
    $("#game-audio")
      .find("source")
      .attr("src", `/static/audio/${data.song}.ogg`);
    $("#game-audio").animate({ volume: 0 }, 1000, function () {
      $("#game-audio").trigger("load").trigger("play");
    });
    $("#game-audio").animate({ volume: 0.05 }, 5000);
  }
  data.options.forEach(function (option) {
    next_scene = option.next_scene ? option.next_scene : -1;
    $("#options").append(
      `
                <button data-id='${option.id}' data-next='${next_scene}' type='button' class='option'>
                    ${option.blurb}
                </button>
                `
    );
  });
  $(".option").click(onOptionClick);
}
