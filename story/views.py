from django.core.exceptions import ObjectDoesNotExist
from django.http.response import Http404
from django.core import serializers
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views import View

from .models import PlayerItem, PlayerLetter, PlayerName, PlayerPath, PlayerSession, PlayerPath, Scene, Item, \
    SceneDialogue, SceneItem
from .forms import PlayForm, LetterForm


def index(request):
    if request.session.get('code', False):
        return redirect('play')
    return redirect('start')


def start(request):
    if request.session.get('code', False):
        return redirect('play')
    form = PlayForm()
    return render(request, 'start.html', {'form': form, 'song': '/static/audio/city-song'})


def handler404(request):
    return render(request, '404.html')


START_DIALOGUE_OPTIONS = [{
    'id': 'continue',
    'blurb': 'CONTINUE'
}]

DIALOGUE_OPTIONS = [
    {
        'id': 'continue',
        'blurb': 'CONTINUE'
    },
    {
        'id': 'back',
        'blurb': 'BACK'
    }]


class ItemAjax(View):
    def get(self, request):
        if not request.is_ajax:
            raise Http404

        item_id = request.GET.get('item')
        item = Item.objects.get(id=item_id)
        return JsonResponse({
            'blurb': item.name,
            'words': item.description,
            'is_letter': item.is_letter
        })


class InventoryAjax(View):
    def get(self, request):
        if not request.is_ajax:
            raise Http404

        code = request.session.get('code', False)
        if code:
            session = PlayerSession.objects.get(code=code)
            # return JsonResponse({
            #     'items': [{
            #         'id': item.id,
            #         'name': item.name,
            #         'image': item.image
            #     } for item in Item.objects.all()]})
            return JsonResponse({
                'items': [{
                    'id': player_item.item.id,
                    'name': player_item.item.name,
                    'image': player_item.item.image
                } for player_item in session.get_items()]})

        raise Http404


class OptionAjax(View):

    def get(self, request):
        if request.is_ajax:
            option = request.GET.get('option')
            scene_id = request.session.get('scene', 0)
            scene = Scene.objects.get(id=scene_id)
            if option == 'continue':
                result = self.__handle_continue(request, scene)
            elif option == 'back':
                result = self.__handle_back(request, scene)
            elif option == 'investigate':
                result = self.__handle_investigate(request, scene)
            elif option == 'restart':
                result = self.__handle_restart(request)
            elif option == 'giveup':
                result = self.__handle_giveup(request)
            elif option == 'proceed':
                result = self.__handle_proceed(
                    request, scene, request.GET.get('next_scene'))
            elif option == 'return':
                result = self.__handle_return(scene)
            else:
                result = self.__handle_option(request, scene, option)
            return result
        else:
            raise Http404

    def __handle_continue(self, request, scene):
        position = request.session.get('dialogue_position')
        if scene.get_dialogue_size() == position + 1:
            del request.session['dialogue_position']
            options = [{'blurb': 'INVESTIGATE', 'id': 'investigate', }] + \
                      [{'blurb': option.blurb if option.blurb is not None else str(option.end_scene),
                        'id': option.id
                        } for option in scene.get_options()]
            if not scene.get_options().exists():
                options = [{'blurb': 'RESTART', 'id': 'restart'},
                           {'blurb': 'GIVE UP', 'id': 'giveup'}]
            return JsonResponse({
                'blurb': scene.name,
                'words': scene.description,
                'options': options
            })
        dialogue = scene.get_dialogue(position + 1)
        request.session['dialogue_position'] = position + 1
        name = PlayerSession.objects.get(code=request.session.get('code')).name.title()
        formatted_dialogue = dialogue.words.replace('PLAYER', name)
        return JsonResponse({
            'blurb': dialogue.blurb,
            'words': formatted_dialogue,
            'options': DIALOGUE_OPTIONS
        })

    def __handle_giveup(self, request):
        session = PlayerSession.objects.get(code=request.session.get('code'))
        current = session.get_current_scene()
        PlayerPath.objects.create(session=session, trial=current.trial, position=current.position + 1,
                                  scene=Scene.objects.get(name='computer'))
        for item in Item.objects.all():
            if not PlayerItem.objects.filter(session=session, item=item).exists() and item.name != 'exotic butters':
                PlayerItem.objects.create(session=session, item=item)
        return JsonResponse({
            'make': True
        })

    def __handle_back(self, request, scene):
        position = request.session.get('dialogue_position')
        dialogue = scene.get_dialogue(position - 1)
        request.session['dialogue_position'] = position - 1
        name = PlayerSession.objects.get(code=request.session.get('code')).name
        formatted_dialogue = dialogue.words.replace('PLAYER', name)
        if position - 1 == 0:
            return JsonResponse({
                'blurb': dialogue.blurb,
                'words': formatted_dialogue,
                'options': START_DIALOGUE_OPTIONS
            })

        return JsonResponse({
            'blurb': dialogue.blurb,
            'words': formatted_dialogue,
            'options': DIALOGUE_OPTIONS
        })

    def __handle_investigate(self, request, scene):
        session = PlayerSession.objects.get(code=request.session.get('code'))
        for item in SceneItem.objects.filter(scene=scene):
            if not PlayerItem.objects.filter(session=session, item=item.item).exists():
                PlayerItem.objects.create(session=session, item=item.item)
        return JsonResponse({
            'blurb': scene.name,
            'words': scene.investigate_description,
            'options': [{
                'id': 'return',
                'blurb': 'RETURN'
            }]
        })

    def __handle_restart(self, request):
        session = PlayerSession.objects.get(code=request.session.get('code'))
        PlayerItem.objects.filter(session=session).delete()
        start = PlayerPath.objects.restart(session)
        request.session['scene'] = start.scene_id
        data = {}
        data.update({'song': start.scene.location.song})
        try:
            dialogue = SceneDialogue.objects.get_first_dialogue(
                session, start.scene)
            request.session['dialogue_position'] = 0
            name = PlayerSession.objects.get(
                code=request.session.get('code')).name
            formatted_dialogue = dialogue.words.replace('PLAYER', name)
            data.update({
                'blurb': dialogue.blurb,
                'words': formatted_dialogue,
                'options': START_DIALOGUE_OPTIONS
            })
        except ObjectDoesNotExist:
            data.update({
                'blurb': start.scene.name,
                'words': start.scene.description,
                'options': [{'blurb': 'INVESTIGATE',
                             'id': 'investigate', }] + [{
                    'blurb': option.blurb,
                    'id': option.id,
                } for option in start.scene.get_options()]
            })
        return JsonResponse(data)

    def __handle_return(self, scene):
        return JsonResponse({
            'blurb': scene.name,
            'words': scene.description,
            'options': [{'blurb': 'INVESTIGATE',
                         'id': 'investigate', }] + [{
                'blurb': option.blurb if option.blurb is not None else str(option.end_scene),
                'id': option.id,
            } for option in scene.get_options()]
        })

    def __handle_proceed(self, request, scene, option):
        new_scene = Scene.objects.get(pk=option)
        session = PlayerSession.objects.get(code=request.session.get('code'))
        request.session['scene'] = new_scene.id
        data = {}
        PlayerPath.objects.create_next_path(session, new_scene)
        if scene.location != new_scene.location:
            data.update({'song': new_scene.location.song})
        try:
            dialogue = SceneDialogue.objects.get_first_dialogue(
                session, new_scene)
            request.session['dialogue_position'] = 0
            name = PlayerSession.objects.get(
                code=request.session.get('code')).name
            formatted_dialogue = dialogue.words.replace('PLAYER', name)
            data.update({
                'blurb': dialogue.blurb,
                'words': formatted_dialogue,
                'options': START_DIALOGUE_OPTIONS
            })
        except ObjectDoesNotExist:
            options = [{'blurb': 'RESTART', 'id': 'restart'},
                       {'blurb': 'GIVEUP', 'id': 'giveup'}] if not new_scene.get_options().exists() else [
                                                                                                             {
                                                                                                                 'blurb': 'INVESTIGATE',
                                                                                                                 'id': 'investigate', }] + [
                                                                                                             {
                                                                                                                 'blurb': option.blurb if option.blurb is not None else str(
                                                                                                                     option.end_scene),
                                                                                                                 'id': option.id,
                                                                                                             } for
                                                                                                             option in
                                                                                                             new_scene.get_options()]
            data.update({
                'blurb': new_scene.name,
                'words': new_scene.description,
                'options': options
            })
        return JsonResponse(data)

    def __handle_option(self, request, scene, option):
        session = PlayerSession.objects.get(code=request.session.get('code'))
        choice = scene.get_options().get(id=option)
        if choice.end_scene == Scene.objects.get(name='alex'):
            return JsonResponse({
                'make': True
            })
        if choice.has_requirements(session):
            return JsonResponse({
                'blurb': choice.blurb,
                'words': choice.description,
                'options': [
                    {
                        'id': 'return',
                        'blurb': 'RETURN'
                    }
                ]
            })
        else:
            return JsonResponse({
                'blurb': choice.blurb,
                'words': choice.success_description,
                'options': [
                    {
                        'id': 'proceed',
                        'blurb': 'PROCEED',
                        'next_scene': choice.end_scene.id
                    },
                    {
                        'id': 'return',
                        'blurb': 'RETURN'
                    }
                ]
            })


def secret(request):
    if not request.session.get('code', False):
        return redirect('/start')
    session = PlayerSession.objects.get(code=request.session.get('code'))
    if not PlayerItem.objects.filter(session=session, item=Item.objects.get(name='exotic butters')).exists():
        PlayerItem.objects.create(session=session, item=Item.objects.get(name='exotic butters'))
    return redirect('/play')


def final(request):
    if not request.session.get('code', False):
        return redirect('/start')
    session = PlayerSession.objects.get(code=request.session.get('code'))
    if session.get_current_scene().scene != Scene.objects.get(name='computer'):
        return redirect('/play')

    for item in Item.objects.all():
        if not PlayerItem.objects.filter(session=session, item=item).exists() and item.name != 'exotic butters':
            PlayerItem.objects.create(session=session, item=item)

    return render(request, 'final.html', {'song': '/static/audio/city-song'})


class PlayView(View):
    def post(self, request):
        form = PlayForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            code = request.session.get('code', form.cleaned_data['code'])
            return self.__get_game(request, code, name)
        return redirect('start', error='form is invalid')

    def __get_game(self, request, code, name=None):
        player_session = self.__get_session(name, code)
        if player_session is None:
            return redirect('start', error='code is not correct')
        game_state = self.__get_state(player_session)
        if game_state.scene == Scene.objects.get(name='computer') and PlayerLetter.objects.filter(
                session=player_session).exists():
            return redirect('final')
        elif game_state.scene == Scene.objects.get(name='alex'):
            return redirect('make')
        game_dialogue = self.__get_dialogue(player_session, game_state)
        if game_dialogue is not None:
            request.session['dialogue_position'] = 0
            formatted_dialogue = game_dialogue.words.replace(
                'PLAYER', player_session.name.upper())
            data = {
                'player': player_session,
                'song': f'/static/audio/{game_state.scene.location.song}',
                'blurb': game_dialogue.blurb,
                'words': formatted_dialogue,
                'options': START_DIALOGUE_OPTIONS
            }
        else:
            options = [{'blurb': 'RESTART', 'id': 'restart'},
                       {'blurb': 'GIVEUP', 'id': 'giveup'}] if not game_state.scene.get_options().exists() else [{
                'blurb': 'INVESTIGATE',
                'id': 'investigate', }] + [
                                                                                                                    {
                                                                                                                        'blurb': option.blurb if option.blurb is not None else str(
                                                                                                                            option.end_scene),
                                                                                                                        'id': option.id,
                                                                                                                    }
                                                                                                                    for
                                                                                                                    option
                                                                                                                    in
                                                                                                                    game_state.scene.get_options()]
            data = {
                'player': player_session,
                'song': f'/static/audio/{game_state.scene.location.song}',
                'blurb': game_state.scene.name,
                'words': game_state.scene.description,
                'options': options
            }
        request.session['code'] = player_session.code
        request.session['scene'] = game_state.scene.id

        return render(request, 'play.html', data)

    def __get_session(self, name, code):
        if name == '' or name is None:
            name = PlayerName.objects.choice().name

        session = PlayerSession.objects.filter(code=code)
        if session.exists():
            return session.first()
        if code == '' or code is None:
            return PlayerSession.objects.get_or_create_session(name, code)
        return None

    def __get_state(self, session):
        return PlayerPath.objects.get_or_create_current_path(session)

    def __get_dialogue(self, session, state):
        try:
            return SceneDialogue.objects.get_first_dialogue(session, state.scene)
        except ObjectDoesNotExist:
            return None

    def get(self, request):
        if request.session.get('code', False):
            return self.__get_game(request, code=request.session.get('code'))
        else:
            return redirect('start')


class MakeView(View):
    def get(self, request, *args, **kwargs):
        if not request.session.get('code', False):
            return redirect('/start')

        session = PlayerSession.objects.get(code=request.session.get('code'))
        if session.get_current_scene().scene != Scene.objects.get(name='computer'):
            return redirect('/play')

        if PlayerLetter.objects.filter(session=session).exists():
            return redirect('/final')

        form = LetterForm()
        return render(request, 'letter.html', {'form': form, 'song': '/static/audio/city-song'})

    def post(self, request, *args, **kwargs):
        if not request.session.get('code', False):
            return redirect('/start')
        session = PlayerSession.objects.get(code=request.session.get('code'))
        if session.get_current_scene().scene != Scene.objects.get(name='computer'):
            return redirect('/play')
        session = PlayerSession.objects.get(code=request.session.get('code'))
        form = LetterForm(request.POST)
        text = '' if not form.is_valid() else form.cleaned_data['text']
        PlayerLetter.objects.create(session=session, letter=text)
        return redirect('final')
