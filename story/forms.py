from django import forms

from .models import PlayerSession


class PlayForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'YOUR NAME (OPTIONAL)', 'class': 'text-field'}), label='', required=False)
    code = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'CODE (OPTIONAL)', 'class': 'text-field'}), label='', required=False)


class LetterForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea(attrs={'class': 'text-area'}), label='', required=False)
