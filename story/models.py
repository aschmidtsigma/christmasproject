from django.db import models

from .managers import PlayerSessionQuerySet, PlayerNameQuerySet, PlayerPathQuerySet, SceneDialogueQuerySet


class PlayerSession(models.Model):
    code = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=75)

    objects = PlayerSessionQuerySet.as_manager()

    def __str__(self):
        return self.code

    def get_items(self):
        return self.playeritem_set.order_by('item__name')

    def get_current_scene(self):
        return self.playerpath_set.latest()


class PlayerName(models.Model):
    name = models.CharField(max_length=75, unique=True)

    objects = PlayerNameQuerySet.as_manager()

    def __str__(self):
        return self.name


class Location(models.Model):
    name = models.CharField(max_length=50, unique=True, null=False, blank=False)
    song = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name


class Item(models.Model):
    name = models.CharField(max_length=50, unique=True, null=False, blank=False)
    description = models.CharField(max_length=500, null=True, blank=True)
    image = models.CharField(max_length=50, default='letter-item.png')
    is_letter = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Scene(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    description = models.CharField(max_length=500, null=True, blank=True)
    investigate_description = models.CharField(max_length=500, null=True, blank=True)

    class Meta:
        unique_together = [['name', 'location']]

    def __str__(self):
        return self.name + ', ' + str(self.location)

    def get_dialogue(self, position):
        return self.scenedialogue_set.get(position=position)

    def get_options(self):
        return self.begin_scene.all()

    def get_dialogue_size(self):
        return self.scenedialogue_set.count()


class PlayerItem(models.Model):
    session = models.ForeignKey(PlayerSession, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)

    class Meta:
        unique_together = [['session', 'item']]

    def __str__(self):
        return str(self.session) + ': ' + str(self.item)


class PlayerLetter(models.Model):
    session = models.ForeignKey(PlayerSession, on_delete=models.CASCADE)
    letter = models.CharField(max_length=5000, null=True, blank=True, default='')

    class Meta:
        unique_together = [['session', 'letter']]

    def __str__(self):
        return str(self.session) + ': ' + str(self.letter)


class PlayerPath(models.Model):
    session = models.ForeignKey(PlayerSession, on_delete=models.CASCADE)
    position = models.PositiveIntegerField()
    scene = models.ForeignKey(Scene, on_delete=models.CASCADE)
    trial = models.PositiveIntegerField()

    objects = PlayerPathQuerySet.as_manager()

    class Meta:
        unique_together = [['session', 'position', 'scene', 'trial']]
        get_latest_by = ['trial', 'position']

    def __str__(self):
        return str(self.session) + ': ' + str(self.position) + ', ' + str(self.scene)


class SceneDialogue(models.Model):
    scene = models.ForeignKey(Scene, on_delete=models.CASCADE)
    position = models.PositiveIntegerField()
    blurb = models.CharField(max_length=100)
    words = models.CharField(max_length=500)

    objects = SceneDialogueQuerySet.as_manager()

    class Meta:
        unique_together = [['scene', 'position']]
        get_latest_by = 'position'

    def __str__(self):
        return str(self.scene) + ': ' + str(self.words)


class SceneEdge(models.Model):
    begin_scene = models.ForeignKey(Scene, on_delete=models.CASCADE, related_name='begin_scene')
    end_scene = models.ForeignKey(Scene, on_delete=models.CASCADE, related_name='end_scene')
    description = models.CharField(max_length=500, null=True, blank=True)
    success_description = models.CharField(max_length=500, null=True, blank=True)
    blurb = models.CharField(max_length=500, null=True, blank=True)

    class Meta:
        unique_together = [['begin_scene', 'end_scene']]

    def __str__(self):
        return '\"' + str(self.begin_scene) + '\" -> \"' + str(self.end_scene) + '\"'

    def has_requirements(self, session):
        for requirement in self.sceneedgerequirment_set.all():
            if not session.playeritem_set.filter(item=requirement.item).exists():
                return True
        return False


class SceneEdgeRequirment(models.Model):
    edge = models.ForeignKey(SceneEdge, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)

    class Meta:
        unique_together = [['edge', 'item']]

    def __str__(self):
        return str(self.edge) + ': ' + str(self.item)


class SceneItem(models.Model):
    scene = models.ForeignKey(Scene, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)

    class Meta:
        unique_together = [['scene', 'item']]

    def __str__(self):
        return str(self.scene) + ': ' + str(self.item)
