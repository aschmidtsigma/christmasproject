from django.contrib import admin
from story.models import PlayerName, Location, Item, Scene, SceneEdge, SceneEdgeRequirment, SceneItem, SceneDialogue, \
    PlayerSession, PlayerPath, PlayerLetter


@admin.register(PlayerName)
class PlayerNameAdmin(admin.ModelAdmin):
    pass


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    pass


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    pass


@admin.register(Scene)
class SceneAdmin(admin.ModelAdmin):
    pass


@admin.register(SceneEdge)
class SceneEdgeAdmin(admin.ModelAdmin):
    pass


@admin.register(SceneEdgeRequirment)
class SceneEdgeRequirmentAdmin(admin.ModelAdmin):
    pass


@admin.register(SceneItem)
class SceneItemAdmin(admin.ModelAdmin):
    pass


@admin.register(PlayerLetter)
class PlayerLetterAdmin(admin.ModelAdmin):
    pass


@admin.register(SceneDialogue)
class SceneDialogueAdmin(admin.ModelAdmin):
    pass


admin.site.register(PlayerSession)
admin.site.register(PlayerPath)
