from django.db import models
from django.core import exceptions
import secrets
import random


class PlayerNameQuerySet(models.QuerySet):

    def choice(self):
        return random.choice(self)


class PlayerSessionQuerySet(models.QuerySet):

    def create_session(self, name):
        code = secrets.token_hex(nbytes=5)
        while self.filter(code=code).exists():
            code = secrets.token_hex(nbytes=5)
        player = self.create(name=name, code=code)
        return player

    def get_or_create_session(self, name, code):
        if self.filter(code=code).exists():
            player = self.get(code=code)
            return player
        else:
            return self.create_session(name=name)


class PlayerPathQuerySet(models.QuerySet):

    def get_or_create_current_path(self, session):
        if self.filter(session=session).exists():
            return self.filter(session=session).latest()
        else:
            return self.create(session=session, position=0, trial=0, scene_id=2)

    def restart(self, session):
        prev_trial = self.filter(session=session).latest().trial
        print('RESTART:', prev_trial)
        return self.create(session=session, position=0, trial=prev_trial + 1, scene_id=2)

    def create_next_path(self, session, scene):
        new_position = self.filter(session=session).latest().position + 1
        trial = self.filter(session=session).latest().trial
        return self.create(session=session, position=new_position, trial=trial, scene=scene)


class SceneDialogueQuerySet(models.QuerySet):

    def get_first_dialogue(self, session, scene):
        max_trial = session.playerpath_set.filter(scene=scene).aggregate(models.Max('trial'))['trial__max']
        if session.playerpath_set.filter(scene=scene, trial=max_trial).count() == 1:
            return self.filter(scene=scene).earliest()
        raise exceptions.ObjectDoesNotExist

    def get_next_dialogue(self, scene, position):
        return self.get(scene=scene, position=position)
