# Generated by Django 3.2.9 on 2021-12-11 18:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story', '0007_alter_sceneedge_blurb'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sceneedge',
            name='blurb',
            field=models.CharField(default='test', max_length=500),
        ),
    ]
